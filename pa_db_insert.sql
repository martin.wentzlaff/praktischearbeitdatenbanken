insert into "mitarbeiter" 
(p_mitarbeiterId, name, titel, rolle, f_softwareId) 
values 
(0001, "Martin", "Junior DevOps", "Developer", 0001);

insert into "mitarbeiter" 
(p_mitarbeiterId, name, titel, rolle, f_softwareId) 
values 
(0002, "Peter", "Senior DevOps", "Developer", 0001);

insert into "mitarbeiter" 
(p_mitarbeiterId, name, titel, rolle, f_softwareId) 
values 
(0003, "Lisa", "Junior Developer", "Developer", 0002);

insert into "mitarbeiter" 
(p_mitarbeiterId, name, titel, rolle, f_softwareId) 
values 
(0001, "Nemi", "Senor Developer", "Developer", 0002);

insert into "software"
(p_softwareId, softwareName, version)
values
(0001, "Maschine2", "V2.3");

insert into "software"
(p_softwareId, softwareName, version)
values
(0002, "Traktor", "V3.4");

insert into "tools"
(toolId, toolName, preis)
values
(0001, "Golang", 14.99);

insert into "tools"
(toolId, toolName, preis)
values
(0002, "PhpMyAdmin", 0.00);

insert into "usesTool"
(f_mitarbeiterId, f_toolId)
values
(0001, 0001);

insert into "usesTool"
(f_mitarbeiterId, f_toolId)
values
(0001, 0002);

insert into "usesTool"
(f_mitarbeiterId, f_toolId)
values
(0002, 0001);

insert into "usesTool"
(f_mitarbeiterId, f_toolId)
values
(0003, 0001);

insert into "usesTool"
(f_mitarbeiterId, f_toolId)
values
(0004, 0001);

insert into "usesTool"
(f_mitarbeiterId, f_toolId)
values
(0004, 0002);
