create table "mitarbeiter" (
	"p_mitarbeiterId" int,
	"name" text,
	"titel" text,
	"rolle" text,
	"f_softwareId" int
);

create table "software" (
	"p_softwareId" int,
	"softwareName" text,
	"version" text
);

create table "tools" (
	"toolId" int,
	"toolName" text,
	"preis" float
);

create table "usesTool" (
	"f_mitarbeiterId" int,
	"f_toolId" int
);
	
